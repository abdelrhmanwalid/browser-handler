package com.abdelrhman.browserhandler;

import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

public class SharedPrefConfigs implements Configs {
    public static final String TAG = SharedPrefConfigs.class.getSimpleName();
    private static final String KEY_DEFAULT = "DEFAULT";
    private SharedPreferences preferences;
    private PackageManager packageManager;

    public SharedPrefConfigs(SharedPreferences preferences, PackageManager packageManager) {
        this.preferences = preferences;
        this.packageManager = packageManager;
    }

    @Override
    public AppConfig updateConfig(ApplicationInfo app, ApplicationInfo browser) {
        AppConfig appConfig = AppConfig.createAppConfig(app, browser);

        if (appConfig.getBrowserPackage() != null) {
            preferences.edit().putString(appConfig.getPackageName(), appConfig.getBrowserPackage()).apply();
        } else {
            remove(appConfig);
        }
        return appConfig;
    }

    @Override
    public AppConfig getConfig(ApplicationInfo app) {

        String packageName = preferences.getString(app.packageName, null);
        ApplicationInfo info = null;
        try {
            info = packageManager.getApplicationInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return AppConfig.createAppConfig(app, info);
    }

    @Override
    public ApplicationInfo getDefaultBrowser() {
        String packageName = preferences.getString(KEY_DEFAULT, null);
        ApplicationInfo info = null;
        try {
            info = packageManager.getApplicationInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "No Default");
        }
        return info;
    }

    @Override
    public void setDefaultBrowser(ApplicationInfo browser) {
        preferences.edit().putString(KEY_DEFAULT, browser.packageName).apply();
    }

    private void remove(AppConfig appConfig) {
        preferences.edit().remove(appConfig.getPackageName()).apply();
    }
}
