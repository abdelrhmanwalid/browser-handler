package com.abdelrhman.browserhandler.home;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.abdelrhman.browserhandler.AppConfig;
import com.abdelrhman.browserhandler.AppInfo;
import com.abdelrhman.browserhandler.Configs;
import com.abdelrhman.browserhandler.R;

import java.util.List;

class ApplicationViewHolder extends RecyclerView.ViewHolder {

    public static final String TAG = ApplicationViewHolder.class.getSimpleName();
    private final ArrayAdapter<Browser> arrayAdapter;
    private List<Browser> browsers;
    private Configs configs;
    private ImageView appImage;
    private TextView appName;
    private Spinner browsersSpinner;
    private AppInfo app;


    ApplicationViewHolder(View itemView, Configs configs, List<Browser> browsers) {
        super(itemView);
        this.configs = configs;
        this.browsers = browsers;
        appImage = itemView.findViewById(R.id.home_app_image);
        appName = itemView.findViewById(R.id.home_app_name);
        browsersSpinner = itemView.findViewById(R.id.home_app_browser);
        arrayAdapter = new ArrayAdapter<>(
                itemView.getContext(),
                android.R.layout.simple_list_item_1,
                browsers
        );
    }

    static int getLayout() {
        return R.layout.home_app_item;
    }

    void bind(AppInfo app) {
        selectedListener.resetCheck();
        this.app = app;

        appImage.setImageDrawable(app.getAppIcon());
        appName.setText(app.getAppName());

        browsersSpinner.setOnItemSelectedListener(null);
        browsersSpinner.setAdapter(arrayAdapter);

        AppConfig config = configs.getConfig(app.getApplicationInfo());
        Browser currentBrowser = new Browser();
        currentBrowser.packageName = config.getBrowserPackage();
        int currentBrowserIndex = browsers.indexOf(currentBrowser);

        browsersSpinner.setSelection(currentBrowserIndex);
        browsersSpinner.setOnItemSelectedListener(selectedListener);
    }

    private CustomItemSelectedListener selectedListener = new CustomItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            super.onItemSelected(parent, view, position, id);
            Browser browser = browsers.get(position);
            configs.updateConfig(app.getApplicationInfo(), browser.applicationInfo);
        }
    };

}
