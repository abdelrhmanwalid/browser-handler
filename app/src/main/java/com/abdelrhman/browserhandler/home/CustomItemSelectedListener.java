package com.abdelrhman.browserhandler.home;

import android.view.View;
import android.widget.AdapterView;

public class CustomItemSelectedListener implements AdapterView.OnItemSelectedListener {
    private boolean firstSelection = true;

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // skip first
        if (firstSelection) {
            firstSelection = false;
            return;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void resetCheck() {
        firstSelection = true;
    }
}
