package com.abdelrhman.browserhandler.home;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.abdelrhman.browserhandler.AppClass;
import com.abdelrhman.browserhandler.AppInfo;
import com.abdelrhman.browserhandler.Configs;
import com.abdelrhman.browserhandler.R;
import com.abdelrhman.browserhandler.SharedPrefConfigs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HomeActivity extends Activity {

    public static final String TAG = HomeActivity.class.getSimpleName();
    private PackageManager packageManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity_home);

        packageManager = getPackageManager();

        ProgressBar progressBar = findViewById(R.id.home_progress);
        progressBar.setVisibility(View.VISIBLE);

        List<AppInfo> apps = getUserApps();
        final List<Browser> browsers = getBrowsers();


        final Configs configs = new SharedPrefConfigs(getSharedPreferences(AppClass.CONFIG, MODE_PRIVATE), packageManager);

        findViewById(R.id.home_app_image).setVisibility(View.GONE);
        TextView appName = findViewById(R.id.home_app_name);
        Spinner browsersSpinner = findViewById(R.id.home_app_browser);

        ArrayAdapter arrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                browsers
        );

        appName.setText("Default Browser");

        browsersSpinner.setAdapter(arrayAdapter);
        ApplicationInfo defaultBrowser = configs.getDefaultBrowser();
        Browser currentBrowser = new Browser();
        currentBrowser.packageName = defaultBrowser != null ? defaultBrowser.packageName : null;
        int currentBrowserIndex = browsers.indexOf(currentBrowser);

        if (currentBrowserIndex != -1)
            browsersSpinner.setSelection(currentBrowserIndex);

        browsersSpinner.setOnItemSelectedListener(new CustomItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                super.onItemSelected(parent, view, position, id);
                Browser browser = browsers.get(position);
                configs.setDefaultBrowser(browser.applicationInfo);
            }
        });


        RecyclerView recyclerView = findViewById(R.id.home_apps);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        ApplicationsAdapter adapter = new ApplicationsAdapter(configs, apps, browsers);
        recyclerView.setAdapter(adapter);

        progressBar.setVisibility(View.GONE);
    }


    private List<AppInfo> getUserApps() {

        List<ApplicationInfo> apps = packageManager.getInstalledApplications(0);
        Collections.sort(apps, new ApplicationInfo.DisplayNameComparator(packageManager));
        List<AppInfo> appInfos = new ArrayList<>(apps.size());
        for (ApplicationInfo app : apps) {
            if (packageManager.getLaunchIntentForPackage(app.packageName) != null) {
                appInfos.add(new AppInfo(app, packageManager));
            }
        }
        return appInfos;
    }

    private List<Browser> getBrowsers() {
        Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://"));
        List<ResolveInfo> apps = packageManager.queryIntentActivities(browserIntent, 0);
        List<Browser> browsers = new ArrayList<>();
        for (ResolveInfo app : apps) {
            Browser browser = new Browser();
            browser.applicationInfo = app.activityInfo.applicationInfo;
            browser.name = app.loadLabel(packageManager).toString();
            browser.packageName = app.activityInfo.packageName;
            browsers.add(browser);
        }
        return browsers;
    }
}
