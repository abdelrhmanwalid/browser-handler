package com.abdelrhman.browserhandler.home;

import android.content.pm.ApplicationInfo;

class Browser {
    String name;
    String packageName;
    ApplicationInfo applicationInfo;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Browser browser = (Browser) o;

        return packageName != null ? packageName.equals(browser.packageName) : browser.packageName == null;

    }

    @Override
    public int hashCode() {
        return packageName != null ? packageName.hashCode() : 0;
    }
}
