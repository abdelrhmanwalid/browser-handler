package com.abdelrhman.browserhandler.home;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abdelrhman.browserhandler.AppInfo;
import com.abdelrhman.browserhandler.Configs;

import java.util.ArrayList;
import java.util.List;

class ApplicationsAdapter extends RecyclerView.Adapter<ApplicationViewHolder> {

    private Configs configs;
    private List<AppInfo> apps;
    private List<Browser> browsers;

    public ApplicationsAdapter(Configs configs, List<AppInfo> apps, List<Browser> browsers) {
        this.configs = configs;
        this.apps = apps;
        this.browsers = new ArrayList<>(browsers);
        Browser browser = new Browser();
        browser.name = "Default";
        this.browsers.add(0, browser);
    }

    @Override
    public ApplicationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(ApplicationViewHolder.getLayout(), parent, false);
        return new ApplicationViewHolder(view, configs, browsers);
    }

    @Override
    public void onBindViewHolder(ApplicationViewHolder holder, int position) {
        holder.bind(apps.get(position));
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }


}
