package com.abdelrhman.browserhandler;

import android.content.pm.ApplicationInfo;

public class AppConfig {
    private String packageName;
    private String browserPackage;

    private AppConfig() {
    }

    public static AppConfig createAppConfig(ApplicationInfo app, ApplicationInfo browser) {
        AppConfig appConfig = new AppConfig();
        appConfig.packageName = app != null ? app.packageName : null;
        appConfig.browserPackage = browser != null ? browser.packageName : null;
        return appConfig;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getBrowserPackage() {
        return browserPackage;
    }
}
