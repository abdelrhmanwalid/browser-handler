package com.abdelrhman.browserhandler;

import android.content.pm.ApplicationInfo;

public interface Configs {
    AppConfig updateConfig(ApplicationInfo app, ApplicationInfo browser);

    AppConfig getConfig(ApplicationInfo app);

    void setDefaultBrowser(ApplicationInfo browser);

    ApplicationInfo getDefaultBrowser();
}
