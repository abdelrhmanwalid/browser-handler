package com.abdelrhman.browserhandler.browserHandler;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.Toast;

import com.abdelrhman.browserhandler.AppClass;
import com.abdelrhman.browserhandler.AppConfig;
import com.abdelrhman.browserhandler.Configs;
import com.abdelrhman.browserhandler.SharedPrefConfigs;

import java.util.List;

public class BrowserHandlerActivity extends Activity {

    public static final String TAG = BrowserHandlerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isServiceEnabled()){
            Toast.makeText(this, "the app needs the accessibility service enabled to work", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            startActivity(intent);
            finish();
            return;
        }

        String packageName = ((AppClass) getApplication()).getApp(0);
        PackageManager packageManager = getPackageManager();

        // get the app config
        Configs configs = new SharedPrefConfigs(
                getSharedPreferences(AppClass.CONFIG, MODE_PRIVATE), packageManager);
        AppConfig appConfig = null;
        try {
            appConfig = configs.getConfig(packageManager.getApplicationInfo(packageName, 0));
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        Intent intent = getIntent();
        // remove the component so our app wont receive the intent
        intent.setComponent(null);

        if (appConfig != null) {
            // make sure the app browser is installed
            String browserPackage = appConfig.getBrowserPackage();
            ApplicationInfo browserInfo = null;
            try {
                browserInfo = packageManager.getApplicationInfo(browserPackage, 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (browserInfo != null) {
                // the app has a browser set
                if (getPackageName().equals(browserInfo.packageName)) {
                    // if the browser is set to out app finish
                    Toast.makeText(this, "dropping intent", Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                } else {
                    // set the browser
                    intent.setPackage(browserInfo.packageName);
                }

            } else {
                // no browser set, falling to the default one
                ApplicationInfo defaultBrowser = configs.getDefaultBrowser();
                if (defaultBrowser != null) {
                    intent.setPackage(defaultBrowser.packageName);
                } else {
                    Toast.makeText(this, "default browser not set", Toast.LENGTH_SHORT).show();
                }
            }
        }

        startActivity(Intent.createChooser(intent, "choose browser"));
        finish();
    }

    private boolean isServiceEnabled() {
        AccessibilityManager am = (AccessibilityManager) getSystemService(ACCESSIBILITY_SERVICE);
        List<AccessibilityServiceInfo> list = am.getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);
        for (AccessibilityServiceInfo accessibilityServiceInfo : list) {
            if (accessibilityServiceInfo.getId().startsWith(getPackageName()))
                return true;
        }
        return false;
    }

}
