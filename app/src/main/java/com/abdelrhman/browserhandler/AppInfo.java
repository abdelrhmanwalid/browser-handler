package com.abdelrhman.browserhandler;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

public class AppInfo {
    private String appName;
    private String packageName;
    private Drawable appIcon;
    private ApplicationInfo applicationInfo;

    public AppInfo(ApplicationInfo applicationInfo, PackageManager packageManager) {
        this.applicationInfo = applicationInfo;
        appName = applicationInfo.loadLabel(packageManager).toString();
        appIcon = applicationInfo.loadIcon(packageManager);
        packageName = applicationInfo.packageName;
    }

    public String getAppName() {
        return appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public ApplicationInfo getApplicationInfo() {
        return applicationInfo;
    }
}

