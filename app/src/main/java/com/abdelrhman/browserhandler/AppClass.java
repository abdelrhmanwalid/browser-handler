package com.abdelrhman.browserhandler;

import android.app.Application;


public class AppClass extends Application {

    public static final String CONFIG = "config";

    private String[] apps;


    public void addApp(String name) {
        if (apps == null) {
            apps = new String[2];
        }
        apps[0] = apps[1];
        apps[1] = name;
    }

    public String getApp(int index){
        if (apps == null) {
            return null;
        }
        return apps[index];
    }
}
